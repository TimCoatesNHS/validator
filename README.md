# Validator V2.0

## A FHIR Validator

This is able to fetch and validate against remote FHIR resources. It caches
fetched resources for a variable number of minutes.

## Basis
Based on the [HAPI FHIR Validator]
(http://hapifhir.io/doc_validation.html#Supplying_your_own_StructureDefinitions)
code.

## Usage
Build as a WAR and run (e.g. with a Context Root of /validator) and then:
- Browse to [http://localhost:8080/validator/](http://localhost:8080/validator/)
to see management interface
- Post a [Parameters](https://www.hl7.org/fhir/parameters.html) resource to for
example:
[http://localhost:8080/validator/fhir/Consent/$validate](http://localhost:8080/validator/fhir/Consent/$validate)

## To Do
- Make fetchURL use the cache rather than calling functions needing to worry, or make ResourceCache auto cache things.
- Do some REAL validation, to see whether it is working!!

## Note
This latest version now ignores any profiles claimed in /resource/meta/profile
if one is either supplied in the parameters resource, or is supplied on the
querystring as described at:
https://www.hl7.org/fhir/resource-operations.html#2.28.7.1.1