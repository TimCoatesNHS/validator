/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nhs.fhir.validator.validation;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.validation.FhirValidator;
import ca.uhn.fhir.validation.IValidatorModule;
import ca.uhn.fhir.validation.SchemaBaseValidator;
import ca.uhn.fhir.validation.ValidationResult;
import ca.uhn.fhir.validation.schematron.SchematronBaseValidator;
import java.util.logging.Logger;
import org.hl7.fhir.dstu3.hapi.validation.DefaultProfileValidationSupport;
import org.hl7.fhir.dstu3.hapi.validation.FhirInstanceValidator;
import org.hl7.fhir.dstu3.hapi.validation.ValidationSupportChain;
import org.hl7.fhir.instance.model.api.IBaseResource;

/**
 *
 * @author tim.coates@nhs.net
 */
public class MyValidator {
    /** Logger we use across this class. **/
    private static final Logger LOG =
            Logger.getLogger(MyValidator.class.getName());

    /** The only instance of this singleton class. **/
    private static volatile MyValidator instance = null;

    /** The underlying HAPI FHIR Validator we load our Validations into. **/
    private final FhirValidator val;


    /**
     * Constructor, only ever called from getInstance().
     *
     * @param newCtx The FHIR Context from the calling servlet.
     *
     */
    protected MyValidator(final FhirContext newCtx) { // Defeats instantiation
        LOG.fine("Creating a Validator");

        /** The FHIR Context. **/
        FhirContext ctx = newCtx;

        val = ctx.newValidator();

        /** One of our Validation modules. **/
        IValidatorModule mod1 = new SchemaBaseValidator(ctx);
        val.registerValidatorModule(mod1);
        LOG.fine(" SchemaBaseValidator registered");

        /** The other one. **/
        IValidatorModule mod2 = new SchematronBaseValidator(ctx);
        val.registerValidatorModule(mod2);
        LOG.fine(" SchematronBaseValidator registered");

        /** The InstanceValidator (validates against profiles. **/
        FhirInstanceValidator instVal;
        instVal = new FhirInstanceValidator();
        val.registerValidatorModule(instVal);
        LOG.fine(" FhirInstanceValidator registered");

        MyInstanceValidator myInstVal = new MyInstanceValidator(ctx);
        /*
         * ValidationSupportChain strings multiple instances of
        IValidationSupport together. The code below is useful because it means
        that when the validator wants to load a StructureDefinition or a
        ValueSet, it will first use DefaultProfileValidationSupport, which loads
        the default HL7 versions. Any StructureDefinitions which are not found
        in the built-in set are delegated to your custom implementation.
         */
        ValidationSupportChain support =
                new ValidationSupportChain(
                        new DefaultProfileValidationSupport(),
                        myInstVal);
        instVal.setValidationSupport(support);
        LOG.fine(" MyInstanceValidator registered");
        LOG.fine("  Validator created");
    }

    /**
     * Used to get our Singleton instance.
     *
     * @param newCtx The FHIR Context
     * @return An instance (the only one) of our Validator objects.
     */
    public static MyValidator getInstance(final FhirContext newCtx) {
        if (instance == null) {
            instance = new MyValidator(newCtx);
        }
        return instance;
    }

    /**
     * Just exposes the ability to Validate a resource.
     *
     * @param resource The resource Object to be validated.
     *
     * The profile to be validated against must be provided in <meta><profile>
     *
     * @return A HAPI ValidationResult object
     */
    public final ValidationResult validateWithResult(
            final IBaseResource resource) {
        ValidationResult vr = val.validateWithResult(resource);
        return vr;
    }

    /**
     * Just exposes the ability to Validate a resource.
     *
     * @param resource The resource to be validated, serialised as a String.
     *
     * The profile to be validated against must be provided in <meta><profile>
     *
     * @return A HAPI ValidationResult object
     */
    public final ValidationResult validateWithResult(final String resource) {
        ValidationResult vr = val.validateWithResult(resource);
        return vr;
    }
}
