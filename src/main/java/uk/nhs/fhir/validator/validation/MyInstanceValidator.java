/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator.validation;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import org.hl7.fhir.dstu3.hapi.ctx.IValidationSupport;
import org.hl7.fhir.dstu3.model.CodeSystem;
import org.hl7.fhir.dstu3.model.CodeSystem.ConceptDefinitionComponent;
import org.hl7.fhir.dstu3.model.StructureDefinition;
import org.hl7.fhir.dstu3.model.ValueSet;
import org.hl7.fhir.dstu3.model.ValueSet.ConceptSetComponent;
import org.hl7.fhir.dstu3.model.ValueSet.ValueSetExpansionComponent;
import org.hl7.fhir.instance.model.api.IBaseResource;
import uk.nhs.fhir.validator.caching.ResourceCache;
import org.hl7.fhir.utilities.validation.ValidationMessage.IssueSeverity;

/**
 * An instanceValidator which implements the HAPI IValidationSupport Interface.
 *
 * This is required to validate against any non core HL7 resources.
 *
 * @author tim.coates@nhs.net
 */
public class MyInstanceValidator implements IValidationSupport {

    /**
     * The logger we use across this class. *
     */
    private static final Logger LOG
            = Logger.getLogger(MyInstanceValidator.class.getName());
    /**
     * The HAPI Fhir Context. *
     */
    private FhirContext ctx = null;
    /**
     * The cache we use to cache remote resources. *
     */
    private ResourceCache cachedSD;
    /**
     * An XML parser to get resources from thir XML serialisation. *
     */
    private IParser parser;

    /**
     * Number of minutes we'll cache resources for.
     */
    private static final int CACHE_MINUTES = 10;

    /**
     * Milliseconds we'll wait for an http connection.
     */
    private static final int CONNECT_TIMEOUT_MILLIS = 50000;

    /**
     * Milliseconds we'll wait to read data over http.
     */
    private static final int READ_TIMEOUT_MILLIS = 50000;

    /**
     * Here we construct a new validator.
     *
     * @param theCtx The FHIR Context, we use this to create a suitable Parser.
     */
    public MyInstanceValidator(final FhirContext theCtx) {
        this.ctx = theCtx;
        this.cachedSD = ResourceCache.getInstance(CACHE_MINUTES);
        parser = ctx.newXmlParser();
    }

    /**
     * Private constructor so one can't be created without having the FHIR
     * context provided.
     *
     */
    private MyInstanceValidator() {
    }

    /**
     * NB: This is NOT IMPLEMENTED.
     *
     * @param theContext The FhirContext we're passed.
     * @param included The Code which is included.
     * @return
     */
    @Override
    public final ValueSetExpansionComponent expandValueSet(
            final FhirContext theContext,
            final ConceptSetComponent included) {
        LOG.fine("MyInstanceValidator has been asked to expand a ValueSet");

        ValueSetExpansionComponent retVal = new ValueSetExpansionComponent();

        Set<String> wantCodes = new HashSet<>();
        for (ValueSet.ConceptReferenceComponent next : included.getConcept()) {
            wantCodes.add(next.getCode());
        }

        CodeSystem system = fetchCodeSystem(theContext, included.getSystem());
        for (ConceptDefinitionComponent next : system.getConcept()) {
            if (wantCodes.isEmpty() || wantCodes.contains(next.getCode())) {
                retVal.addContains().setSystem(
                        included.getSystem())
                        .setCode(next.getCode())
                        .setDisplay(next.getDisplay());
            }
        }

        return retVal;
    }

    /**
     * Method in which we should pre-fetch all Conformance Resources. We have no
     * presumed Conformances, therefore we simply return an empty list.
     *
     * @param theContext FHIR Context.
     * @return A List (Currently empty) of the Conformace Resources we
     * recognise.
     */
    @Override
    public final List<IBaseResource> fetchAllConformanceResources(
            final FhirContext theContext) {
        LOG.fine("MyInstanceValidator asked to fetch all ConformanceResources");
        List<IBaseResource> crList = new ArrayList<>();
        return crList;
    }

    /**
     * Meethod to pre-fetch all StructureDefinition resources that we are happy
     * to work with. As we're open minded, we return an empty list, and deal
     * with them as they're chucked at us.
     *
     * @param theContext FHIR context.
     * @return A List (currently empty) of StructureDefinitions
     */
    @Override
    public final List<StructureDefinition> fetchAllStructureDefinitions(
            final FhirContext theContext) {
        LOG.fine("MyInstanceValidator asked to fetch all StructureDefinitions");
        List<StructureDefinition> sdList = new ArrayList<>();
        return sdList;
    }

    /**
     * Method to fetch a CodeSystem based on a provided URL. Tries to fetch it
     * from remote server. If it succeeds, it caches it in our global cache.
     *
     * @param theContext FHIR Context
     * @param theSystem The CodeSystem URL
     * @return Returns the retrieved FHIR Resource object or null
     */
    @Override
    public final CodeSystem fetchCodeSystem(
            final FhirContext theContext, final String theSystem) {
        logIt(
                "MyInstanceValidator asked to fetch Code System: {0}",
                theSystem);
        CodeSystem newCS;

        if (cachedSD.getResource(theSystem) == null) {
            LOG.fine(" Not cached");
            String response = fetchURL(theSystem);
            if (response != null) {
                LOG.fine("  Retrieved");
                cachedSD.putResource(theSystem, response);
                LOG.fine("   Cached");
            }
        }
        if (cachedSD.getResource(theSystem) == null) {
            LOG.fine("  Couldn't fetch it, so returning null");
            return null;
        } else {
            newCS = (CodeSystem) parser.parseResource(
                    cachedSD.getResource(theSystem));
            LOG.fine(" Provided from cache");
        }
        return newCS;
    }

    /**
     * Method to retrieve any old FHIR Resource based on a URL.
     *
     * @param <T> The type to return.
     * @param theContext FHIR Conetxt.
     * @param theClass The Class type we're being asked for.
     * @param theUrl The URL to fetch from.
     * @return Returns an arbitrary FHIR Resource object, or null if it can't be
     * retrieved.
     */
    @Override
    public final <T extends IBaseResource> T fetchResource(
            final FhirContext theContext,
            final Class<T> theClass,
            final String theUrl) {
        logIt(
                "MyInstanceValidator asked to fetch Resource: {0}",
                theUrl);

        if (theUrl.startsWith("http://hl7.org/fhir/")
                || theUrl.startsWith("https://hl7.org/fhir/") // ||
                //theUrl.startsWith("http://www.hl7.org/fhir/") ||
                //theUrl.startsWith("https://www.hl7.org/fhir/")
                ) {
            LOG.fine("  Returning null as it's an HL7 one");
            return null;
        }

        if (cachedSD.getResource(theUrl) == null) {
            String response = fetchURL(theUrl);
            if (response != null) {
                LOG.fine("  About to parse response into a Resource");
                cachedSD.putResource(theUrl, response);
                LOG.log(Level.FINE, "  Resource added to cache: {0}", theUrl);
            } else {
                LOG.log(Level.WARNING, "  No data returned from: {0}", theUrl);
            }
        } else {
            LOG.log(Level.FINE, "  This URL was already loaded: {0}", theUrl);
        }
        String theResourceContent = cachedSD.getResource(theUrl);
        IBaseResource newResource = null;
        if (theResourceContent != null) {
            newResource = parser.parseResource(theResourceContent);
        }
        return (T) newResource;
    }

    /**
     * Method to fetch a remote StructureDefinition resource.
     *
     * Caches results.
     *
     * @param theCtx FHIR Context
     * @param theUrl The URL to fetch from
     * @return The StructureDefinition resource or null
     */
    @Override
    public final StructureDefinition fetchStructureDefinition(
            final FhirContext theCtx,
            final String theUrl) {
        logIt(
                "MyInstanceValidator asked to fetch StructureDefinition: {0}",
                theUrl);

        if (theUrl.startsWith("http://hl7.org/fhir/")
                || theUrl.startsWith("https://hl7.org/fhir/") // ||
                //theUrl.startsWith("http://www.hl7.org/fhir/") ||
                //theUrl.startsWith("https://www.hl7.org/fhir/")
                ) {
            LOG.fine("  Returning null as it's an HL7 one");
            return null;
        }

        if (cachedSD.getResource(theUrl) == null) {
            String response = fetchURL(theUrl);
            LOG.fine("  About to parse response into a StructureDefinition");

            cachedSD.putResource(theUrl, response);
            logIt("  StructureDefinition now added to the cache: {0}", theUrl);
        } else {
            LOG.log(Level.FINE, "  This URL was already loaded: {0}", theUrl);
        }
        StructureDefinition sd
                = (StructureDefinition) parser.parseResource(
                        cachedSD.getResource(theUrl));
        return sd;
    }

    /**
     * Method to say whether the code system is supported by this validation.
     *
     * Currently works on the basis of if it can retrieve the CodeSystem, then
     * yes it's happy to support it.
     *
     * @param theContext FHIR Context
     * @param theSystem The URL of the CodeSystem
     * @return Boolean result
     */
    @Override
    public final boolean isCodeSystemSupported(
            final FhirContext theContext,
            final String theSystem) {
        logIt(
                "MyInstanceValidator asked if CodeSystem is supported: {0}",
                theSystem);
        if (cachedSD.getResource(theSystem) == null) {
            // Not loaded yet, but CAN we load it?
            LOG.fine(" Not YET found in local cache, trying to fetch it");
            String response = fetchURL(theSystem);
            if (response != null) {
                cachedSD.putResource(theSystem, response);
            }
            if (cachedSD.getResource(theSystem) == null) {
                LOG.fine("  STILL not in local cache, so returning FALSE");
                return false;
            }
        }
        LOG.fine(" Found in local cache, so returning TRUE");
        return true;
    }

    /**
     * Validates a Code and Display against a CodeSystem. Simply checks whether
     * the System holds the Code, and if so, does the Display value match?
     *
     * @param theContext FHIR Context
     * @param theCodeSystem CodeSystem URL
     * @param theCode Code value
     * @param theDisplay Display value
     * @return Whether a match was found.
     */
    @Override
    public final CodeValidationResult validateCode(
            final FhirContext theContext,
            final String theCodeSystem,
            final String theCode,
            final String theDisplay) {
        logIt("MyInstanceValidator asked to validate code: {0}", theCode);

        if (cachedSD.getResource(theCodeSystem) == null) {
            // Not loaded yet, but CAN we load it?
            LOG.fine(" Not YET found in local cache, so trying to fetch it");
            String response = fetchURL(theCodeSystem);
            if (response != null) {
                cachedSD.putResource(theCodeSystem, response);
            }
            if (cachedSD.getResource(theCodeSystem) == null) {
                LOG.fine("  STILL not in local cache, so returning null");
                return new CodeValidationResult(
                        IssueSeverity.ERROR,
                        "Failed to retrieve system: " + theCodeSystem);
            }
        }

        CodeSystem cs = parser.parseResource(
                CodeSystem.class,
                cachedSD.getResource(theCodeSystem));
        List<ConceptDefinitionComponent> concepts = cs.getConcept();

        for (ConceptDefinitionComponent item : concepts) {
            if (item.getCode().equals(theCode)) {
                if (item.getDisplay().equals(theDisplay)) {
                    // Here we have succeeded
                    CodeValidationResult cvr
                            = new CodeValidationResult(
                                    IssueSeverity.NULL,
                                    "Success");
                    return cvr;
                }
            }
        }
        return new CodeValidationResult(
                IssueSeverity.ERROR,
                "Code not found in system.");
    }

    /**
     * Utility function used to fetch both StructureDefinitions and Resources
     * (as they're the same thing) from a remote URL.
     *
     * @param theUrl The URL represented as a string.
     * @return The http response as a string.
     */
    private String fetchURL(final String theUrl) {
        logIt("  This URL not yet loaded: {0}", theUrl);

        StringBuilder result = new StringBuilder();

        try {
            URL url = new URL(theUrl);
            try {
                HttpURLConnection conn
                        = (HttpURLConnection) url.openConnection();
                // We need to supply a header value here. otherwise the
                // fhir.nhs.uk server assumes we're a browser, and returns us a
                // pretty html view of the requested resource.
                conn.setRequestProperty("Accept", "xml");
                conn.setConnectTimeout(CONNECT_TIMEOUT_MILLIS);
                conn.setReadTimeout(READ_TIMEOUT_MILLIS);
                LOG.fine("    Connected");
                try {
                    conn.setRequestMethod("GET");
                    try {
                        BufferedReader rd
                                = new BufferedReader(
                                        new InputStreamReader(
                                                conn.getInputStream(),
                                                Charset.forName("UTF-8")));
                        try {
                            int httpCode = conn.getResponseCode();
                            if (httpCode == SC_OK) {
                                LOG.fine("    Got a 200 status");
                                String line;
                                try {
                                    while ((line = rd.readLine()) != null) {
                                        result.append(line);
                                    }
                                    LOG.fine("    No more data");
                                    rd.close();
                                } catch (IOException ex) {
                                    warnIt(
                                "IOException 1 caught trying to read from: {0}",
                                            theUrl);
                                    LOG.warning(ex.getMessage());
                                }
                            } else {
                                warnIt("  http status code was: {0}", httpCode);
                            }
                        } catch (IOException ex) {
                            warnIt(
                                    "IOException 2 caught trying to fetch: {0}",
                                    theUrl);
                            LOG.warning(ex.getMessage());
                        }
                    } catch (IOException ex) {
                        warnIt(
                                "IOException 3 caught trying to fetch: {0}",
                                theUrl);
                    }
                } catch (ProtocolException ex) {
                    warnIt(
                            "ProtocolException 4 caught trying to fetch: {0}",
                            theUrl);
                    LOG.warning(ex.getMessage());
                }
            } catch (IOException ex) {
                warnIt(
                        "IOException 5 caught trying to fetch: {0}",
                        theUrl);
                LOG.warning(ex.getMessage());
            }
        } catch (MalformedURLException ex) {
            warnIt(
                    "MalformedURLException 6 caught trying to fetch: {0}",
                    theUrl);
            LOG.warning(ex.getMessage());
        }
        if (result.length() > 0) {
            return result.toString();
        } else {
            return null;
        }
    }

    /**
     * Method to log a message using a message template.
     *
     * @param log The message template eg "Msg is: {0}".
     * @param value The object to drop into the {0} placeholder.
     */
    private void logIt(final String log, final Object value) {
        LOG.log(Level.FINE, log, value);
    }

    /**
     * Method to log a warning message using a message template.
     *
     * @param log The message template eg "Msg is: {0}".
     * @param value The object to drop into the {0} placeholder.
     */
    private void warnIt(final String log, final Object value) {
        LOG.log(Level.WARNING, log, value);
    }
}
