/**
 * This is the package to hold the real Validation comopnents. It includes an
 * InstanceValidator, which is happy to fetch external / remote resources, and
 * a wrapper class which makes it available as a Singleton.
 *
 */

package uk.nhs.fhir.validator.validation;
