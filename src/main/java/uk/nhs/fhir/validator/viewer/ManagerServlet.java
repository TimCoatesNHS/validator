/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator.viewer;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.ResourceNotFoundException;
import uk.nhs.fhir.validator.caching.CachedObject;
import uk.nhs.fhir.validator.caching.ResourceCache;

/**
 * Servlet to provide an ability to see what the Validator is doing.
 *
 * @author tim.coates@nhs.net
 */
@WebServlet(urlPatterns = {"/*"}, displayName = "Management")
public class ManagerServlet extends HttpServlet {

    /** Length we ignore when getting the value from querystring. */
    private static final int MINS_WORD_LENGTH = 5;

    /**
     * Error message we use wherever needed. *
     */
    private static final String NO_TPL_ERR = "Unable to load resource: {0}";

    /**
     * Our logger which is used across this class. *
     */
    private static final Logger LOG
            = Logger.getLogger(ManagerServlet.class.getName());
    /**
     * A set of Velocity properties to allow us to load templates from the
     * classpath. *
     */
    private final Properties velocityProps;

    /**
     * Our constructor, which just sets up Velocity for use elsewhere.
     */
    public ManagerServlet() {
        LOG.fine("Creating ManagerServlet");
        velocityProps = new Properties();
        velocityProps.setProperty("resource.loader", "class");
        velocityProps.setProperty(
        "class.resource.loader.class",
        "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(velocityProps);
    }

    /**
     * This handles all requests, routing them internally to create the right
     * response.
     *
     * @param request The incoming http request.
     * @param response The html we respond with.
     */
    @Override
    public final void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {
        String requestName = request.getPathInfo();
        LOG.log(Level.FINE, "Asked for page: {0}", requestName);

        String responseContent = "";

        switch (requestName) {
            case "/clearcache":
                responseContent = clearcache();
                break;

            case "/showcache":
                responseContent = showcache();
                break;

            case "/settimeout":
                String minutes = request.getQueryString();
                if (minutes == null) {
                    responseContent = settimeout();
                } else {
                    int mins = Integer.parseInt(
                            minutes.substring(MINS_WORD_LENGTH));
                    settimeout(mins);
                }
                break;

            default:
                responseContent = home();
        }
        try {
            response.getWriter().print(responseContent);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "IOException caught{0}", ex.getMessage());
        }
    }

    /**
     * Displays the homepage, well this actually builds it rather than
     * displaying it, that's obviously the browser's job.
     *
     * @return HTML representation of the home page, with a list of available
     * options.
     */
    public final String home() {
        LOG.fine("Serving home page");
        StringWriter sw = new StringWriter();
        VelocityContext context = new VelocityContext();
        Template template;
        context.put("title", "FHIR Validator - Home");

        try {
            template = Velocity.getTemplate("home.html");
            template.merge(context, sw);
        } catch (ResourceNotFoundException rnfe) {
            LOG.log(Level.WARNING, NO_TPL_ERR, rnfe.getMessage());
        }
        return sw.toString();
    }

    /**
     * Flush the cache, then show the home page.
     *
     * @return The results of the home() method.
     */
    public final String clearcache() {
        LOG.fine("Asked to clear the cache...");
        ResourceCache myCache = ResourceCache.getInstance();
        myCache.flush();
        return home();
    }

    /**
     * Shows the items currently cached. NB: Some may have already expired.
     *
     * @return HTML page with a list of cached objects, showing their URL (link)
     * and size in bytes.
     */
    public final String showcache() {
        LOG.fine("Asked to show the cache contents");
        ResourceCache myCache = ResourceCache.getInstance();

        int minutes = myCache.getMinutes();

        HashMap map = (HashMap) myCache.getHashMap();

        Set ks = map.keySet();

        VelocityContext context = new VelocityContext();
        Template template;
        context.put("title", "FHIR Validator - Show-cache");
        context.put("minutes", minutes);
        StringWriter sw = new StringWriter();

        HashMap m = new HashMap();
        for (Iterator it = ks.iterator(); it.hasNext();) {
            String k = (String) it.next();
            CachedObject cached = (CachedObject) map.get(k);
            String v = Integer.toString(cached.getResource().length());
            m.put(k, v);
        }
        context.put("items", m);

        try {
            template = Velocity.getTemplate("showcache.html");
            template.merge(context, sw);
        } catch (ResourceNotFoundException rnfe) {
            LOG.log(Level.WARNING, NO_TPL_ERR, rnfe.getMessage());
        }
        return sw.toString();
    }

    /**
     * Method allows setting of the cache timeout value to a new number of
     * minutes.
     *
     * @param mins New number of minutes to set the cache to.
     * @return Returns a web page showing that the cache has been updated.
     */
    public final String settimeout(final int mins) {
        LOG.fine("Setting Timeout");

        VelocityContext context = new VelocityContext();
        Template template;
        context.put("title", "FHIR Validator - Set Timeout");
        StringWriter sw = new StringWriter();
        String templateName;

        ResourceCache.getInstance(mins);

        templateName = "settimeout2.html";
        context.put("minutes", mins);

        try {
            template = Velocity.getTemplate(templateName);

            template.merge(context, sw);
        } catch (ResourceNotFoundException rnfe) {
            LOG.log(Level.WARNING, NO_TPL_ERR, rnfe.getMessage());
        }
        return sw.toString();
    }

    /**
     * Method shows a form to allow setting the cache timeout to a new number of
     * minutes.
     *
     * @return Returns an html form to allow a new value to be set for our cache
     * timeout.
     */
    public final String settimeout() {
        LOG.fine("Set Timeout form");

        VelocityContext context = new VelocityContext();
        Template template;
        context.put("title", "FHIR Validator - Set Timeout");
        StringWriter sw = new StringWriter();
        String templateName;

        templateName = "settimeout1.html";
        try {
            template = Velocity.getTemplate(templateName);
            template.merge(context, sw);
        } catch (ResourceNotFoundException rnfe) {
            LOG.log(Level.WARNING,
                    NO_TPL_ERR,
                    rnfe.getMessage());
        }
        return sw.toString();
    }
}
