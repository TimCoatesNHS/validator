/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator.caching;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class to clean out our cache, checks every nn seconds.
 *
 * @author tim.coates@nhs.net
 */
public class CacheCleaner implements Runnable {

    /** Default cleanup timeout. */
    private static final int DEFAULT_SECONDS = 45;

    /** Number of milliseconds in a second. */
    private static final int MILLIS_PER_SECOND = 1000;

    /** Only instance of this class. */
    private static volatile CacheCleaner instance = null;

    /** The logger we use for this class. */
    private static final Logger LOG =
            Logger.getLogger(CacheCleaner.class.getName());

    /** The number of seconds between cleanups. */
    private int seconds = DEFAULT_SECONDS;

    /** Flag to show we should keep running. */
    private boolean running = true;

    /**
     * Method to get the Singleton instance of this class.
     *
     * @return The one and only.
     */
    public static CacheCleaner getInstance() {
        LOG.fine("CacheCLeaner requested with default");
        if (instance == null) {
            instance = new CacheCleaner();
        }
        return instance;
    }

    /**
     * Method to get the Singleton instance of this class.
     *
     * @param newSeconds The number of seconds between cleaning runs we want.
     * @return The one and only.
     */
    public static CacheCleaner getInstance(final int newSeconds) {
        LOG.log(Level.FINE, "CacheCLeaner requested with: {0}", newSeconds);
        if (instance == null) {
            instance = new CacheCleaner(newSeconds);
        } else {
            instance.setTimeout(newSeconds);
        }
        return instance;
    }

    /**
     * Constructor which creates this object using default value.
     *
     */
    protected CacheCleaner() {
        LOG.fine("Creating CacheCleaner with default timeout");
    }

    /**
     * Constructor which creates this object with a given timeout.
     *
     * @param newSeconds Number of seconds to wait between cleaning the cache.
     */
    protected CacheCleaner(final int newSeconds) {
        LOG.log(Level.FINE, "Creating CacheCLeaner with: {0}", newSeconds);
        this.seconds = newSeconds;
    }

    /**
     * Method to allow us to tell this thread to stop.
     *
     */
    public final void halt() {
        LOG.fine("Cache cleaner will halt");
        running = false;
    }

    /**
     * This is where the Cleaner does the work.
     *
     */
    @Override
    public final void run() {

        LOG.fine("CacheCleaner running");

        // Now we loop until we're stopped.
        while (running) {
            try {
                Thread.sleep(this.seconds * MILLIS_PER_SECOND);
                doClean();
            } catch (InterruptedException ex) {
                LOG.fine("CacheCleaner interrupted");
                break;
            }
        }
        LOG.fine(" * CacheCleaner finishing");
    }

    /**
     * Method which actually cleans up the cache, removing any stale objects.
     *
     */
    private void doClean() {

        // First we get the cache to be cleaned.
        ResourceCache thecache = ResourceCache.getInstance();
        LOG.fine("Cleaning up...");

        // WE're going to put the stale keys in an ArrayList.
        java.util.ArrayList al = new ArrayList();

        // Iterate through the HashMap keys.
        Iterator<String> it1 = thecache.getHashMap().keySet().iterator();
        while (it1.hasNext() && running) {
            String key = it1.next();
            // Get the cached object.
            CachedObject co = thecache.getHashMap().get(key);
            if (co.stale()) {

                // If it was stale, add it to our ArrayList.
                al.add(key);
            }
        }

        // Now step through the ArralList, and remove items with keys in there.
        for (int i = 0; i < al.size(); i++) {
            if (running) {
                LOG.fine(" deleting");
                thecache.getHashMap().remove(al.get(i));
            }
        }
        LOG.fine("...clean up done");
    }

    /**
     * Method to set the timeout between cleaning runs.
     *
     * @param newSeconds Number of seconds we want to run after.
     */
    private void setTimeout(final int newSeconds) {
        this.seconds = newSeconds;
    }
}
