/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator.caching;

import java.sql.Timestamp;

/**
 * Dead simple class to represent things we're caching, contains a timestamp to
 * allow expiry to be implemented.
 *
 * Used by uk.nhs.fhir.validator.utils.ResourceCache.
 *
 * @author tim.coates@nhs.net
 */
public class CachedObject {
    /** The resource being cached, serialised as a String. **/
    private final String resource;
    /** Timestamp of when we cached it, to check for staleness. **/
    private final Timestamp timestamp;
    /** The number of minutes this object should be cached for. **/
    private final int cacheMinutes;

    /** Number of milliseconds in a minute. */
    private static final int MILLIS = 60000;

    /**
     * Constructor to create a new cached object.
     *
     * @param newResource The String resource we're caching.
     * @param newMinutes The number of minutes we'll cache this for.
     */
    public CachedObject(final String newResource, final int newMinutes) {
        this.cacheMinutes = newMinutes;
        this.resource = newResource;
        this.timestamp = new Timestamp(System.currentTimeMillis());
    }

    /**
     * Method for getting the cached Resource itself.
     *
     * @return Returns the String resource we've cached.
     */
    public final String getResource() {
        return this.resource;
    }

    /**
     * Returns whether a specific item is stale or not, and is therefore due
     * for purging from the cache.
     *
     * @return Boolean result.
     */
    public final boolean stale() {
        Timestamp expiry = new Timestamp(
                System.currentTimeMillis() - (MILLIS * cacheMinutes));
        return this.timestamp.before(expiry);
    }
}
