/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator.caching;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class to hold resources we've downloaded from remote servers. Includes
 * StructureDefinitions, Extensions, CodeSystems etc etc.
 *
 * Created as a Singleton, instantiated by getInstance
 *
 * Marked as final, as Constructor starts a new Thread, sub-classing this would
 * likely break;
 *
 * @author tim.coates@nhs.net
 */
public final class ResourceCache {

    /** The logger we use across this class. **/
    private static final Logger LOG =
            Logger.getLogger(ResourceCache.class.getName());

    /** Only instance of the singleton class. **/
    private static volatile ResourceCache instance = null;

    /** The embedded HashMap of cached resources. **/
    private final HashMap<String, CachedObject> cachedSD;

    /** Default time we'll cache for. */
    private static final int DEFAULT_CACHE_TIME = 10;

    /** The number of minutes we will cache for. **/
    private int myMinutes = DEFAULT_CACHE_TIME;

    /** Class we use to clean up stale objects. */
    private final CacheCleaner myCleaner;

    /** Thread to keep track of our cleaner thread. */
    private Thread cleanerThread = null;

    /**
     * Protected constructor, so externals can't arbitrarily create a new
     * instances (as this should be a Singleton).
     *
     */
    protected ResourceCache() { // Used to defeat instantiation.
        logIt("Creating ResourceCache with default expiry:{0}", myMinutes);
        cachedSD = new HashMap<>();

        /** Set up the cleaning process. */
        myCleaner = CacheCleaner.getInstance();
        cleanerThread = new Thread(myCleaner);
        cleanerThread.start();

    }

    /**
     * Protected constructor, which overrides the default cache timeout. Is
     * protected to ensure this is a Singleton.
     *
     * @param newMinutes Number of minutes to cache objects for in the
     * created instance.
     */
    protected ResourceCache(final int newMinutes) {
        logIt("Creating a ResourceCache with expiry:{0}", newMinutes);
        cachedSD = new HashMap<>();

        /** Set up the cleaning process. */
        myCleaner = CacheCleaner.getInstance();
        cleanerThread = new Thread(myCleaner);
        cleanerThread.start();

        this.myMinutes = newMinutes;
    }

    /**
     * The main way that someone will create one of these caches. It uses the
     * default value for cache timeout. In turn calls the protected constructor
     * as required, or hands out the existing instance.
     *
     * @return An instance (should be THE ONLY instance) of this Class.
     */
    public static ResourceCache getInstance() {
        LOG.fine("Someone asking for the ResourceCache...");
        if (instance == null) {
            LOG.fine(" Creating a new one");
            instance = new ResourceCache();
        } else {
            LOG.fine(" Supplying existing one");
        }
        return instance;
    }

    /**
     * The main way that someone will create one of these caches. It specifies a
     * new value for cache timeout. In turn calls the protected constructor
     * as required, or changes the timeout and hands out the existing instance.
     *
     * @param newMinutes Number of minutes to cache items for.
     * @return An instance (should be THE ONLY instance) of this Class.
     */
    public static ResourceCache getInstance(final int newMinutes) {
        logIt("Request for a new ResourceCache with expiry: {0}", newMinutes);
        if (instance == null) {
            LOG.fine(" Creating a new one");
            instance = new ResourceCache(newMinutes);
        } else {
            LOG.fine(" Supplying existing one");
        }
        instance.setMinutes(newMinutes);
        return instance;
    }

    /**
     * Puts a resource into our cache, giving it an expiry time.
     *
     * @param key The URL of the resource being saved, against which it's
     * indexed.
     * @param resource The FHIR resource being cached.
     */
    public void putResource(final String key, final String resource) {
        CachedObject obj = new CachedObject(resource, myMinutes);
        cachedSD.put(key, obj);
    }

    /**
     * Gets an item from our cache, but only if it's not yet gone stale.
     *
     * If it has gone stale, it deletes the item.
     *
     * @param key The URL of the resource we want back.
     * @return The FHIR Resource or null if not cached (or was stale).
     */
    public String getResource(final String key) {
        LOG.log(Level.FINE, "ResourceCache asked for object: {0}", key);
        CachedObject obj = cachedSD.get(key);
        if (obj != null) {
            if (obj.stale()) {
                //cachedSD.remove(key);
                LOG.fine(" Stale (will be deleted)");
                return null;
            } else {
                LOG.fine(" Found (will be returned)");
                return obj.getResource();
            }
        } else {
            LOG.fine(" Not found (ResourceCache can't help with this)");
            return null;
        }
    }

    /**
     * Method to assign a new Minutes value to a pre existing cache if
     * getInstance is called with a new Value.
     *
     * @param newMinutes The new number of minutes to cache resources for.
     */
    private void setMinutes(final int newMinutes) {
        this.myMinutes = newMinutes;
    }

    /**
     * Method to get the minutes that this (only) instance will cache for.
     *
     * @return Number of minutes
     */
    public int getMinutes() {
        return instance.myMinutes;
    }

    /**
     * Method which allows us to empty the cache.
     */
    public void flush() {
        logit("Cache is being flushed, size: {0}", cachedSD.size());
        cachedSD.clear();
        logit("Cache was flushed, size: {0}", cachedSD.size());
    }

    /**
     * Gets the underlying hashmap of keys & values, so we can see the cached
     * resources.
     *
     * Also used to get the Map for cleaning out stale objects.
     *
     * @return The cache hashmap
     */
    public HashMap<String, CachedObject> getHashMap() {
        return this.cachedSD;
    }

    /**
     * Method to get rid of this ResourceCache, and stop it's cleaner.
     *
     */
    public void destroy() {
        try {
            myCleaner.halt();
            cleanerThread.join();
        } catch (InterruptedException ex) {
            logIt("Interrupted: {0}", ex.getMessage());
        }
        instance = null;
    }

    /**
     * Simple method to log a message template.
     *
     * @param log The Message template eg " Message is: {0}".
     * @param value The value to plug into the {0} placeholder.
     */
    private static void logIt(final String log, final Object value) {
        LOG.log(Level.FINE, log, value);
    }

    /**
     * Simple method to log a message template.
     *
     * @param log The Message template eg " Message is: {0}".
     * @param value The value to plug into the {0} placeholder.
     */
    private void logit(final String log, final int value) {
        LOG.log(Level.FINE, log, value);
    }
}
