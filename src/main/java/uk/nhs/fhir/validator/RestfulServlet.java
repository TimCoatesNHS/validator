/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator;

import uk.nhs.fhir.validator.ResourceProvider.GenericResourceProvider;
import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import org.hl7.fhir.dstu3.model.Consent;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.StructureDefinition;
import org.hl7.fhir.dstu3.model.ValueSet;
import org.hl7.fhir.dstu3.model.OperationDefinition;
import org.hl7.fhir.dstu3.model.Appointment;
import org.hl7.fhir.dstu3.model.FamilyMemberHistory;
import org.hl7.fhir.dstu3.model.Medication;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.Condition;
import org.hl7.fhir.dstu3.model.MedicationRequest;
import org.hl7.fhir.dstu3.model.Device;
import org.hl7.fhir.dstu3.model.PractitionerRole;
import org.hl7.fhir.dstu3.model.Practitioner;
import org.hl7.fhir.dstu3.model.MessageHeader;
import org.hl7.fhir.dstu3.model.Contract;
import org.hl7.fhir.dstu3.model.Organization;
import org.hl7.fhir.dstu3.model.OperationOutcome;
import org.hl7.fhir.dstu3.model.DocumentReference;
import org.hl7.fhir.dstu3.model.Immunization;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.Flag;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.HealthcareService;
import org.hl7.fhir.dstu3.model.AllergyIntolerance;
import org.hl7.fhir.dstu3.model.CapabilityStatement;
import org.hl7.fhir.dstu3.model.Procedure;
import org.hl7.fhir.dstu3.model.ListResource;
import org.hl7.fhir.dstu3.model.Composition;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.RelatedPerson;
import org.hl7.fhir.dstu3.model.Schedule;
import org.hl7.fhir.dstu3.model.Slot;
import org.hl7.fhir.dstu3.model.MedicationStatement;
import org.hl7.fhir.dstu3.model.Location;

/**
 * This is the actual Servlet, which hosts a set of ResourceProviders for each
 * Resource type that we're handling.
 *
 * @author tim.coates@nhs.net
 */
@WebServlet(urlPatterns = {"/fhir/*"}, displayName = "FHIR Validator")
public class RestfulServlet extends RestfulServer {

    /**
     * The logger we use across this class. *
     */
    private static final Logger LOG
            = Logger.getLogger(RestfulServlet.class.getName());

    /**
     * The HAPI Fhir context. *
     */
    private final FhirContext ctx = FhirContext.forDstu3();

    /**
     * Here is where the Servlet is first initialised.
     *
     * @throws ServletException
     */
    @Override
    protected final void initialize() throws ServletException {
        LOG.info("Initialising servlet");
        List<IResourceProvider> rpList = new ArrayList<>();

        // This list is all of the STU3 resource type on fhir.nhs.uk
        rpList.add(new GenericResourceProvider(ctx, StructureDefinition.class));
        rpList.add(new GenericResourceProvider(ctx, Appointment.class));
        rpList.add(new GenericResourceProvider(ctx, FamilyMemberHistory.class));
        rpList.add(new GenericResourceProvider(ctx, Medication.class));
        rpList.add(new GenericResourceProvider(ctx, Encounter.class));
        rpList.add(new GenericResourceProvider(ctx, Condition.class));
        rpList.add(new GenericResourceProvider(ctx, MedicationRequest.class));
        rpList.add(new GenericResourceProvider(ctx, Device.class));
        rpList.add(new GenericResourceProvider(ctx, PractitionerRole.class));
        rpList.add(new GenericResourceProvider(ctx, Practitioner.class));
        rpList.add(new GenericResourceProvider(ctx, MessageHeader.class));
        rpList.add(new GenericResourceProvider(ctx, Contract.class));
        rpList.add(new GenericResourceProvider(ctx, Organization.class));
        rpList.add(new GenericResourceProvider(ctx, OperationOutcome.class));
        rpList.add(new GenericResourceProvider(ctx, DocumentReference.class));
        rpList.add(new GenericResourceProvider(ctx, Immunization.class));
        rpList.add(new GenericResourceProvider(ctx, Parameters.class));
        rpList.add(new GenericResourceProvider(ctx, Patient.class));
        rpList.add(new GenericResourceProvider(ctx, Flag.class));
        rpList.add(new GenericResourceProvider(ctx, Observation.class));
        rpList.add(new GenericResourceProvider(ctx, HealthcareService.class));
        rpList.add(new GenericResourceProvider(ctx, AllergyIntolerance.class));
        rpList.add(new GenericResourceProvider(ctx, CapabilityStatement.class));
        rpList.add(new GenericResourceProvider(ctx, Procedure.class));
        rpList.add(new GenericResourceProvider(ctx, ListResource.class));
        rpList.add(new GenericResourceProvider(ctx, Composition.class));
        rpList.add(new GenericResourceProvider(ctx, Bundle.class));
        rpList.add(new GenericResourceProvider(ctx, RelatedPerson.class));
        rpList.add(new GenericResourceProvider(ctx, Schedule.class));
        rpList.add(new GenericResourceProvider(ctx, Slot.class));
        rpList.add(new GenericResourceProvider(ctx, MedicationStatement.class));
        rpList.add(new GenericResourceProvider(ctx, Location.class));
        rpList.add(new GenericResourceProvider(ctx, Consent.class));

        rpList.add(new GenericResourceProvider(ctx, ValueSet.class));
        rpList.add(new GenericResourceProvider(ctx, OperationDefinition.class));
    //resourceProviders.add(new GenericResourceProvider(ctx, Extension.class));

        setResourceProviders(rpList);
        LOG.info("Created server to handle the configured resources.");
    }

    /**
     * Method to return a list of resources we're here to handle. This is only
     * currently used in the unit tests.
     *
     * @return A list of (class names of) the Resource types we can handle.
     */
    public final List<String> getResources() {
        List<String> resourceNames = new ArrayList<>();
        Collection<IResourceProvider> rpList = this.getResourceProviders();

        for (IResourceProvider item : rpList) {
            resourceNames.add(item.getResourceType().getCanonicalName());
        }
        return resourceNames;
    }
}
