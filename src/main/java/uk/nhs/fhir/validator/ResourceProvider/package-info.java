/**
 * This is the package to hold the ResourceProviders to handle different
 * FHIR Resources.
 *
 * As it turns out, we only need one, as it is passed the type to Provide for at
 * the point of Construction.
 *
 */

package uk.nhs.fhir.validator.ResourceProvider;
