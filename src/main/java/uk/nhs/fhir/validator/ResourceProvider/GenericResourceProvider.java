/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator.ResourceProvider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Validate;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.ValidationModeEnum;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.validation.SingleValidationMessage;
import ca.uhn.fhir.validation.ValidationResult;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.OperationOutcome;
import org.hl7.fhir.instance.model.api.IBaseResource;
import uk.nhs.fhir.validator.validation.MyValidator;

/**
 * This class is able to act as a ResourceProvider for any FHIR STU3 resource
 * type.
 *
 * The required type is passed in via the Constructor.
 *
 * It only implements the Validate and Read operations; Read is only there as a
 * placeholder, it doesn't actually work! Validate does work, it's the main
 * purpose of this whole project.
 *
 * @author tim.coates@nhs.net
 */
public class GenericResourceProvider implements IResourceProvider {

    /**
     * Our logger used throughout this class. *
     */
    private static final Logger LOG = Logger.getLogger(
            GenericResourceProvider.class.getName());

    /**
     * The name of the Resource type that an instance of this class serves. *
     */
    private String typeName;
    /**
     * The Class type of the Resource this instance serves for. *
     */
    private Class myClass;
    /**
     * The FhirContext passed all through this application. *
     */
    private FhirContext ctx;

    /**
     * Length of the bit we need to strip off QueryString (profile=[value]).
     */
    private static final int LENGTH_OF_PROFILE = 8;

    /**
     * Length we need to strip off the UriType=[.
     */
    private static final int LENGTH_OF_URI_PREFIX = 8;

    /**
     * Constructor to create one of these. The type it represents is set by the
     * supplied Class.
     *
     * @param newCtx The parent FhirContext passed in from the actual server
     * @param newClass Sets what resource type this instance will provide for.
     */
    public GenericResourceProvider(
            final FhirContext newCtx,
            final Class<? extends IBaseResource> newClass) {
        this.myClass = newClass;
        this.typeName = myClass.getName();
        ctx = newCtx;
        LOG.log(Level.FINE, "Created resource provider for: {0}", typeName);
    }

    /**
     * Private constructor to prevent the ability to create one with no type.
     */
    private GenericResourceProvider() {
    }

    /**
     * Returns the type of resource being provided for by this instance.
     *
     * @return A class of the type we're set up to provide for.
     */
    @Override
    public final Class<? extends IBaseResource> getResourceType() {
        return myClass;
    }

    /**
     * This is the only operation we claim to provide, but for any class which
     * implements IBaseResource.
     *
     * @param theResource This is the resource to be validated.
     *
     * @param theMode This is the mode of validation, needs to be one of
     * (CREATE, UPDATE, DELETE) Determines whether THIS server would accept the
     * supplied resource to have THIS action applied on THIS server.
     *
     * @param profileName The profile against which we're validating.
     * @param theRequest The raw HTTP request.
     * @param theResponse The raw HTTP response.
     *
     * @return Returns the outcome of the validation...
     */
    @Validate
    public final MethodOutcome validateResource(
            @ResourceParam final IBaseResource theResource,
            @Validate.Mode final ValidationModeEnum theMode,
            @Validate.Profile final String profileName,
            final HttpServletRequest theRequest,
            final HttpServletResponse theResponse) {

        MethodOutcome retVal = new MethodOutcome();

        LOG.fine("Validating a resource...");
        if (theMode != null) {
            LOG.log(Level.FINE, " Validation mode is: {0}", theMode.name());
        } else {
            LOG.fine(" Validation mode not supplied");
        }

        // Make sure the profile to validate against is in /meta/profile element
        IBaseResource myResource = setProfileToUse(
                theResource,
                profileName,
                theRequest);

        ValidationResult res
                = MyValidator.getInstance(ctx).validateWithResult(myResource);

        if (res.isSuccessful()) {
            LOG.fine("Resource Validation succeeded");
            /*
            List<SingleValidationMessage> messages = res.getMessages();
            for (SingleValidationMessage next : messages) {
                LOG.log(Level.WARNING,
                        "Location: {0}", next.getLocationString());
                LOG.log(Level.WARNING, "Severity: {0}", next.getSeverity());
                LOG.log(Level.WARNING, "Message:  {0}\n", next.getMessage());
            }
            OperationOutcome oo = (OperationOutcome) res.toOperationOutcome();
            retVal.setOperationOutcome(oo);
             */
        } else {
            LOG.warning("Validation failure, details follow:");
            List<SingleValidationMessage> messages = res.getMessages();
            for (SingleValidationMessage next : messages) {
                LOG.log(Level.WARNING,
                        "Location: {0}", next.getLocationString());
                LOG.log(Level.WARNING, "Severity: {0}", next.getSeverity());
                LOG.log(Level.WARNING, "Message:  {0}\n", next.getMessage());
            }
            OperationOutcome oo = (OperationOutcome) res.toOperationOutcome();
            retVal.setOperationOutcome(oo);
        }
        return retVal;
    }

    /**
     * The Read interaction, to get a specific instance of a FHIR resource.
     *
     * NB: This is not implemented.
     *
     * @param theId The ID of the resource being requested.
     * @return The requested resource. Currently always returns null.
     */
    @Read()
    public final IBaseResource getResourceById(@IdParam final IdType theId) {
        IBaseResource retVal = null;
        LOG.log(Level.FINE,
                "Had a Read request for item: {0} of type: {1}",
                new Object[]{theId.getIdPart(), this.typeName});
        return retVal;
    }

    /**
     * Method to determine what profile we should validate against.
     *
     * Tries the following in sequence: Parameter from the supplied parameters
     * resource. Profile=[] from Query String.
     * <meta><profile> from the Resource being validated.
     *
     * @param theResource The resource to be validated.
     * @param profileName The profilename from Parameters resource.
     * @param theRequest The http request.
     * @return Returns the Resource, with <meta><profile> set to the selected
     * profile URL.
     *
     */
    protected final IBaseResource setProfileToUse(
            final IBaseResource theResource,
            final String profileName,
            final HttpServletRequest theRequest) {
        String selected = "";
        IBaseResource returnResource = theResource;
        String profile = profileName;

        int profileCount = returnResource.getMeta().getProfile().size();

        // First choice is the parameter profile name.
        if (profile != null) {
            if (profile.equals("")) {
                profile = null;
            }
        }
        if (profile != null) {
            String pStr = profile.substring(LENGTH_OF_URI_PREFIX);
            pStr = pStr.substring(0, pStr.length() - 1);
            selected = pStr;

        } else {
            // Now we try and find one in the QueryString.
            String queryString = theRequest.getQueryString();

            if (queryString == null) {
                queryString = "";
            }

            String queryStringLowerCase = queryString.toLowerCase(Locale.UK);

            if (queryStringLowerCase.contains("profile=")) {
                queryString = queryString.substring(
                        queryStringLowerCase.indexOf("profile=")
                                + LENGTH_OF_PROFILE);

                if (queryString.contains("&")) {
                    queryString = queryString.substring(
                            0,
                            queryString.indexOf('&')
                    );
                }
                selected = queryString;
            } else {
                // No so we hopefully have one in <meta><profile> ?
                if (profileCount > 0) {
                    selected = returnResource
                            .getMeta()
                            .getProfile()
                            .get(0)
                            .getValue();
                }
            }
        }
        // Remove any from <meta><profile>.
        for (int i = 0; i < profileCount; i++) {
            returnResource.getMeta().getProfile().remove(i);
        }
        // And add in the selected one.
        if (selected != null) {
            returnResource.getMeta().addProfile(selected);
        } else {
            returnResource = null;
        }
        return returnResource;
    }
}
