/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator.viewer;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import uk.nhs.fhir.validator.caching.ResourceCache;

/**
 *
 * @author tim.coates@nhs.net
 */
public class ManagerServletTest {

    public ManagerServletTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of home method, of class ManagerServlet.
     */
    @Test
    public void testHome() {
        System.out.println("home");
        ManagerServlet instance = new ManagerServlet();
        String expResult = "<html>\n" +
                            "    <head>\n" +
                            "        <title>FHIR Validator - Home</title>\n" +
                            "        <style>\n" +
                            "            body { font-family: \"Arial\"; }\n" +
                            "        </style>\n" +
                            "    </head>\n" +
                            "    <body>\n" +
                            "        <h1>Validator Cache Management</h1>\n" +
                            "        <p>Options</p>\n" +
                            "        <div>\n" +
                            "            <ul>\n" +
                            "                <li><a href=\"showcache\">Show cache contents</a></li>\n" +
                            "                <li><a href=\"clearcache\">Clear cache</a></li>\n" +
                            "                <li><a href=\"settimeout\">Set cache timeout minutes</a></li>\n" +
                            "            </ul>\n" +
                            "        </div>\n" +
                            "    </body>\n" +
                            "</html>";
        String result = instance.home();
        assertEquals(expResult, result);
    }

    /**
     * Test of clearcache method, of class ManagerServlet.
     */
    @Test
    public void testClearcache() {
        System.out.println("clearcache");
        ManagerServlet instance = new ManagerServlet();
        String expResult = "<html>\n" +
                            "    <head>\n" +
                            "        <title>FHIR Validator - Home</title>\n" +
                            "        <style>\n" +
                            "            body { font-family: \"Arial\"; }\n" +
                            "        </style>\n" +
                            "    </head>\n" +
                            "    <body>\n" +
                            "        <h1>Validator Cache Management</h1>\n" +
                            "        <p>Options</p>\n" +
                            "        <div>\n" +
                            "            <ul>\n" +
                            "                <li><a href=\"showcache\">Show cache contents</a></li>\n" +
                            "                <li><a href=\"clearcache\">Clear cache</a></li>\n" +
                            "                <li><a href=\"settimeout\">Set cache timeout minutes</a></li>\n" +
                            "            </ul>\n" +
                            "        </div>\n" +
                            "    </body>\n" +
                            "</html>";
        String result = instance.clearcache();
        assertEquals(expResult, result);
    }

    /**
     * Test of showcache method, of class ManagerServlet.
     */
    @Test
    public void testShowcache() {
        System.out.println("showcache");
        ManagerServlet instance = new ManagerServlet();
        ResourceCache myCache = ResourceCache.getInstance(10);
        myCache.flush();
        String expResult = "<html>\n" +
                            "    <head>\n" +
                            "        <title>FHIR Validator - Show-cache</title>\n" +
                            "        <style>\n" +
                            "            body { font-family: \"Arial\"; }\n" +
                            "            table#t01 {\n" +
                            "                background-color: #f1f1c1;\n" +
                            "            }\n" +
                            "            table#t01 tr:nth-child(even) {\n" +
                            "                background-color: #eee;\n" +
                            "            }\n" +
                            "            table#t01 tr:nth-child(odd) {\n" +
                            "                background-color: #fff;\n" +
                            "            }\n" +
                            "            table#t01 th {\n" +
                            "                color: white;\n" +
                            "                background-color: blue;\n" +
                            "            }\n" +
                            "        </style>\n" +
                            "    </head>\n" +
                            "    <body>\n" +
                            "        <h1><a href=\"/validator/\">Validator Cache Management</a></h1>\n" +
                            "        <p>Caching the following Items for: 10 minutes</p>\n" +
                            "        <p>NB: Expiry times for cached objects are unknown</p>\n" +
                            "        <div>\n" +
                            "            <table id=\"t01\">\n" +
                            "                <tr>\n" +
                            "                    <th width=\"500\">Key</th>\n" +
                            "                    <th width=\"200\">Size</th>\n" +
                            "                </tr>\n" +
                            "                            </table>\n" +
                            "        </div>\n" +
                            "    </body>\n" +
                            "</html>";
        String result = instance.showcache();
        assertEquals(expResult, result);
    }

        /**
     * Test of showcache method, of class ManagerServlet.
     */
    @Test
    public void testShowcache_1() {
        System.out.println("showcache_1");
        ResourceCache myCache = ResourceCache.getInstance(1);
        ManagerServlet instance = new ManagerServlet();
        String expResult = "<html>\n" +
                            "    <head>\n" +
                            "        <title>FHIR Validator - Show-cache</title>\n" +
                            "        <style>\n" +
                            "            body { font-family: \"Arial\"; }\n" +
                            "            table#t01 {\n" +
                            "                background-color: #f1f1c1;\n" +
                            "            }\n" +
                            "            table#t01 tr:nth-child(even) {\n" +
                            "                background-color: #eee;\n" +
                            "            }\n" +
                            "            table#t01 tr:nth-child(odd) {\n" +
                            "                background-color: #fff;\n" +
                            "            }\n" +
                            "            table#t01 th {\n" +
                            "                color: white;\n" +
                            "                background-color: blue;\n" +
                            "            }\n" +
                            "        </style>\n" +
                            "    </head>\n" +
                            "    <body>\n" +
                            "        <h1><a href=\"/validator/\">Validator Cache Management</a></h1>\n" +
                            "        <p>Caching the following Items for: 1 minutes</p>\n" +
                            "        <p>NB: Expiry times for cached objects are unknown</p>\n" +
                            "        <div>\n" +
                            "            <table id=\"t01\">\n" +
                            "                <tr>\n" +
                            "                    <th width=\"500\">Key</th>\n" +
                            "                    <th width=\"200\">Size</th>\n" +
                            "                </tr>\n" +
                            "                                <tr>\n" +
                            "                    <td><a href=\"Key\">Key</a></td>\n" +
                            "                    <td>1136</td>\n" +
                            "                </tr>\n" +
                            "                            </table>\n" +
                            "        </div>\n" +
                            "    </body>\n" +
                            "</html>";
        myCache.putResource("Key", expResult);
        String result = instance.showcache();
        assertEquals(expResult, result);
        myCache.flush();
    }

    /**
     * Test of settimeout method, of class ManagerServlet.
     */
    @Test
    public void testSettimeout_int() {
        System.out.println("settimeout");
        int mins = 2;
        ManagerServlet instance = new ManagerServlet();
        String expResult = "<html>\n" +
                            "    <head>\n" +
                            "        <title>FHIR Validator - Set Timeout</title>\n" +
                            "        <style>\n" +
                            "            body { font-family: \"Arial\"; }\n" +
                            "        </style>\n" +
                            "    </head>\n" +
                            "    <body>\n" +
                            "        <h1><a href=\"/validator/\">Validator Cache Management</a></h1>\n" +
                            "        <p>Cache timeout set to 2 minutes</p>\n" +
                            "    </body>\n" +
                            "</html>";
        String result = instance.settimeout(mins);
        assertEquals(expResult, result);
    }

    /**
     * Test of settimeout method, of class ManagerServlet.
     */
    @Test
    public void testSettimeout_0args() {
        System.out.println("settimeout");
        ManagerServlet instance = new ManagerServlet();
        String expResult = "<html>\n" +
                            "    <head>\n" +
                            "        <title>FHIR Validator - Set Timeout</title>\n" +
                            "        <style>\n" +
                            "            body { font-family: \"Arial\"; }\n" +
                            "        </style>\n" +
                            "    </head>\n" +
                            "    <body>\n" +
                            "        <h1>Validator Cache Management</h1>\n" +
                            "        <p>Options</p>\n" +
                            "        <div>\n" +
                            "            <form action =\"/validator/settimeout\">\n" +
                            "                New minutes: <input value=\"10\" id=\"mins\" name=\"mins\" type=\"text\" /><br />\n" +
                            "                <input type=\"submit\" />\n" +
                            "            </form>\n" +
                            "        </div>\n" +
                            "    </body>\n" +
                            "</html>";
        String result = instance.settimeout();
        assertEquals(expResult, result);
    }
    
}
