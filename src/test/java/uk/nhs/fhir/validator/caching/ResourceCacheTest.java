/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator.caching;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tim.coates@nhs.net
 */
public class ResourceCacheTest {
    
    public ResourceCacheTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class ResourceCache.
     */
    @Test
    public void testGetInstance_0args() {
        System.out.println("getInstance");
        ResourceCache result = ResourceCache.getInstance();
        assertNotNull(result);
    }

    /**
     * Test of getInstance method, of class ResourceCache.
     */
    @Test
    public void testGetInstance_int() {
        System.out.println("getInstance");
        int newMinutes = 10;
        ResourceCache result = ResourceCache.getInstance(newMinutes);
        assertNotNull(result);
        result.destroy();
    }

    /**
     * Test of putResource method, of class ResourceCache.
     */
    @Test
    public void testPutResource() {
        System.out.println("putResource");
        String key = "KeyABC";
        String resource = "ValueABC";
        ResourceCache instance = ResourceCache.getInstance();
        instance.putResource(key, resource);
        String result = instance.getResource(key);
        assertEquals(resource, result);
        instance.destroy();
    }

    /**
     * Test of getResource method, of class ResourceCache.
     */
    @Test
    public void testGetResource() {
        System.out.println("getResource");
        String key = "Key123";
        String resource = "Value123";
        ResourceCache instance = ResourceCache.getInstance();
        instance.putResource(key, resource);
        String result = instance.getResource(key);
        assertEquals(resource, result);
        instance.destroy();
    }

    /**
     * Test of getMinutes method, of class ResourceCache.
     */
    @Test
    public void testGetMinutes() {
        System.out.println("getMinutes");
        ResourceCache instance = ResourceCache.getInstance(12);
        int expResult = 12;
        int result = instance.getMinutes();
        assertEquals(expResult, result);
        instance.destroy();
        try {
            Thread.sleep(750);
        }
        catch (InterruptedException ex) {
            Logger.getLogger(ResourceCacheTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
