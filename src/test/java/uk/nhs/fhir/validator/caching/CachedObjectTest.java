/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator.caching;

import uk.nhs.fhir.validator.caching.CachedObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tim.coates:nhs.net
 */
public class CachedObjectTest {
    
    public CachedObjectTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getResource method, of class CachedObject.
     */
    @Test
    public void testGetResource() {
        System.out.println("getResource");
        CachedObject instance = new CachedObject("Some text to be cached", 1);
        String expResult = "Some text to be cached";
        String result = instance.getResource();
        assertEquals(expResult, result);
    }

    /**
     * Test of stale method, of class CachedObject.
     */
    @Test
    public void testStale() {
        System.out.println("stale");
        CachedObject instance = new CachedObject("Some text to be cached for zero minutes", 0);
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(CachedObjectTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        boolean expResult = true;
        boolean result = instance.stale();
        assertEquals(expResult, result);
    }
    
}
