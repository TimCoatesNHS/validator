/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator.caching;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tim.coates@nhs.net
 */
public class CacheCleanerTest {

    public CacheCleanerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {

    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class CacheCleaner.
     */
    @Test
    public void testGetInstance_0args() {
        System.out.println("getInstance");
        CacheCleaner result = CacheCleaner.getInstance();
        assertNotNull(result);
        result.halt();
    }

    /**
     * Test of getInstance method, of class CacheCleaner.
     */
    @Test
    public void testGetInstance_int() {
        System.out.println("getInstance");
        int newSeconds = 10;
        CacheCleaner result = CacheCleaner.getInstance(newSeconds);
        assertNotNull(result);
        result.halt();
    }    
}
