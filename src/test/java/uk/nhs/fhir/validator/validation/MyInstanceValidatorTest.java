/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator.validation;

import ca.uhn.fhir.context.FhirContext;
import java.util.List;
import org.hl7.fhir.dstu3.hapi.ctx.IValidationSupport;
import org.hl7.fhir.dstu3.model.CodeSystem;
import org.hl7.fhir.dstu3.model.CodeType;
import org.hl7.fhir.dstu3.model.StructureDefinition;
import org.hl7.fhir.dstu3.model.ValueSet;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.utilities.validation.ValidationMessage.IssueSeverity;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tim.coates@nhs.net
 */
public class MyInstanceValidatorTest {
    FhirContext theContext;
    MyInstanceValidator instance;
    
    public MyInstanceValidatorTest() {
        theContext = FhirContext.forDstu3();
        instance = new MyInstanceValidator(theContext);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of expandValueSet method, of class MyInstanceValidator.
     */
    @Test
    public void testExpandValueSet() {
        System.out.println("expandValueSet");
        
        CodeType code = new CodeType();
        code.setValue("A4");
        ValueSet.ConceptReferenceComponent t = new ValueSet.ConceptReferenceComponent(code);
        t.setDisplay("Disoriented");
        ValueSet.ConceptSetComponent theInclude = new ValueSet.ConceptSetComponent();
        theInclude.addConcept(t);
        theInclude.setSystem("https://stu3.simplifier.net/open/CodeSystem/afc6cce3-73de-4007-90b0-7b8c0826943b?_format=xml");
        int expResult = 1;
        ValueSet.ValueSetExpansionComponent result = instance.expandValueSet(theContext, theInclude);
        assertEquals(result.getContains().size(), expResult);
        assertEquals(result.getContains().get(0).getCode(), code.getValue());
    }

    /**
     * Test of fetchAllConformanceResources method, of class MyInstanceValidator.
     */
    @Test
    public void testFetchAllConformanceResources() {
        System.out.println("fetchAllConformanceResources");
        
        List<IBaseResource> result = instance.fetchAllConformanceResources(theContext);
        assertEquals(0, result.size());
    }

    /**
     * Test of fetchAllStructureDefinitions method, of class MyInstanceValidator.
     */
    @Test
    public void testFetchAllStructureDefinitions() {
        System.out.println("fetchAllStructureDefinitions");
        
        List<StructureDefinition> result = instance.fetchAllStructureDefinitions(theContext);
        assertEquals(0, result.size());
    }

    /**
     * Test of fetchCodeSystem method, of class MyInstanceValidator.
     */
    @Test
    public void testFetchCodeSystem() {
        System.out.println("fetchCodeSystem");
        
        String theSystem = "https://stu3.simplifier.net/open/CodeSystem/afc6cce3-73de-4007-90b0-7b8c0826943b?_format=xml";
        CodeSystem result = instance.fetchCodeSystem(theContext, theSystem);
        assertEquals("v2 Ambulatory Status", result.getName());
    }

    /**
     * Test of fetchResource method, of class MyInstanceValidator.
     */
    @Test
    public void testFetchHL7Resource() {
        System.out.println("FetchResource (HL7)");
        
        // Will return null, as OUR fetcher refuses to fetch HL7 resources...
        Object expResult = null;
        Object result = instance.fetchResource(theContext, StructureDefinition.class, "https://hl7.org/fhir/Patient");
        assertEquals(expResult, result);
    }

    /**
     * Test of fetchResource method, of class MyInstanceValidator.
     */
    @Test
    public void testFetchNHSResource() {
        System.out.println("FetchResource (NHS)");
        
        StructureDefinition result = instance.fetchResource(theContext, StructureDefinition.class, "https://fhir.nhs.uk/STU3/StructureDefinition/Extension-NDOP-Proxy-1");
        assertEquals("http://hl7.org/fhir/StructureDefinition/Extension", result.getBaseDefinition());
    }

    /**
     * Test of fetchStructureDefinition method, of class MyInstanceValidator.
     */
    @Test
    public void testFetchStructureDefinition() {
        System.out.println("fetchStructureDefinition (NHS)");
        
        String theUrl = "https://fhir.nhs.uk/STU3/StructureDefinition/Extension-NDOP-Proxy-1";
        StructureDefinition result = instance.fetchStructureDefinition(theContext, theUrl);
        String expResult = "1.0.0";
        assertEquals(expResult, result.getVersion());
    }


    /**
     * Test of fetchStructureDefinition method, of class MyInstanceValidator.
     */
    @Test
    public void testFetchHL7StructureDefinition() {
        System.out.println("fetchStructureDefinition (HL7)");
        
        String theUrl = "http://hl7.org/fhir/Patient";
        // Will return null as we refuse to process HL7 URLs...
        StructureDefinition expResult = null;
        StructureDefinition result = instance.fetchStructureDefinition(theContext, theUrl);
        assertEquals(expResult, result);
    }
    /**
     * Test of isCodeSystemSupported method, of class MyInstanceValidator.
     */
    @Test
    public void testIsCodeSystemSupported() {
        System.out.println("isCodeSystemSupported (Good)");
        
        String theSystem = "https://fhir.nhs.uk/STU3/CodeSystem/NDOP-OptOutSource-1";
        boolean expResult = true;
        boolean result = instance.isCodeSystemSupported(theContext, theSystem);
        assertEquals(expResult, result);
    }

    /**
     * Test of isCodeSystemSupported method, of class MyInstanceValidator.
     */
    @Test
    public void testIsBadCodeSystemSupported() {
        System.out.println("isCodeSystemSupported (Bad)");
        
        String theSystem = "https://fhir.nhs.uk/STU3/CodeSystem/NonExistent";
        boolean expResult = false;
        boolean result = instance.isCodeSystemSupported(theContext, theSystem);
        assertEquals(expResult, result);
    }

    /**
     * Test of validateCode method, of class MyInstanceValidator.
     */
    @Test
    public void testGoodValidateCode() {
        System.out.println("validateCode (Good)");
        
        String theCodeSystem = "https://stu3.simplifier.net/open/CodeSystem/afc6cce3-73de-4007-90b0-7b8c0826943b?_format=xml";
        String theCode = "A5";
        String theDisplay = "Vision impaired";
        IValidationSupport.CodeValidationResult result = instance.validateCode(theContext, theCodeSystem, theCode, theDisplay);
        String msg = result.getMessage();
        IssueSeverity is = result.getSeverity();
        assertEquals(IssueSeverity.NULL, is);
        assertEquals("Success", msg);
    }
    
    /**
     * Test of validateCode method, of class MyInstanceValidator.
     */
    @Test
    public void testBadValidateCode() {
        System.out.println("validateCode (Bad)");
        
        String theCodeSystem = "https://stu3.simplifier.net/open/CodeSystem/afc6cce3-73de-4007-90b0-7b8c0826943b?_format=xml";
        String theCode = "A5";
        String theDisplay = "Should be okay";
        IValidationSupport.CodeValidationResult expResult = null;
        IValidationSupport.CodeValidationResult result = instance.validateCode(theContext, theCodeSystem, theCode, theDisplay);
        String msg = result.getMessage();
        assertEquals("Code not found in system.", msg);
    }
}
