/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator.validation;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.validation.ResultSeverityEnum;
import ca.uhn.fhir.validation.SingleValidationMessage;
import ca.uhn.fhir.validation.ValidationResult;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tim.coates@nhs.net
 */
public class MyValidatorTest {

    static String theGoodResource = "";
    static String theBadResource = "";
    static String failMsg = "Profile http://hl7.org/fhir/StructureDefinition/"
            + "Patient, Element 'Patient.birthDate: max allowed = 1, but found 2";

    public MyValidatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class MyValidator.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        FhirContext newCtx = FhirContext.forDstu3();
        MyValidator result = MyValidator.getInstance(newCtx);
        assertNotNull(result);
    }

    /**
     * Test of the Validator itself.
     *
     * Tests validating a known good resource, checking that it succeeds.
     *
     */
    @Test
    public void testValidator_Pass_String() {
        System.out.println("testValidator_Pass_String");
        try {
            theGoodResource = IOUtils.toString(
                    this.getClass().getResourceAsStream("patient-example.xml"),
                    "UTF-8"
            );
        }
        catch (IOException ex) {
            Logger.getLogger(MyValidatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        FhirContext newCtx = FhirContext.forDstu3();
        MyValidator instance = MyValidator.getInstance(newCtx);
        ValidationResult res = instance.validateWithResult(theGoodResource);
        assertEquals(true, res.isSuccessful());
    }

    /**
     * Test of the Validator itself. This tests a patient resource with two
     * birthDate elements, which breaks cardinality rules.
     *
     */
    @Test
    public void testValidator_Fail_String() {
        System.out.println("testValidator_Fail_String (Load a resource as a String)");
        try {
            theBadResource = IOUtils.toString(
                    this.getClass().getResourceAsStream("bad-patient-example.xml"),
                    "UTF-8"
            );
        }
        catch (IOException ex) {
            Logger.getLogger(MyValidatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        FhirContext newCtx = FhirContext.forDstu3();
        MyValidator instance = MyValidator.getInstance(newCtx);
        ValidationResult res = instance.validateWithResult(theBadResource);
        // Assert that it failed validation
        assertEquals(false, res.isSuccessful());

        // Assert thee were two problems
        assertEquals(2, res.getMessages().size());

        SingleValidationMessage errorMsg0 = res.getMessages().get(0);
        SingleValidationMessage errorMsg1 = res.getMessages().get(1);

        // Assert the location of the first problem
        assertEquals(errorMsg0.getLocationLine().intValue(), 80);
        assertEquals(errorMsg0.getLocationCol().intValue(), 35);

        // Assert it was an ERROR
        assertEquals(errorMsg0.getSeverity(), ResultSeverityEnum.ERROR);

        // Assert the location of the second problem
        assertEquals(errorMsg1.getLocationLine().intValue(), 0);
        assertEquals(errorMsg1.getLocationCol().intValue(), 0);

        // Assert it was an ERROR
        assertEquals(errorMsg1.getSeverity(), ResultSeverityEnum.ERROR);

        // Assert the second problem's message
        assertEquals(failMsg, errorMsg1.getMessage());
    }

    /**
     * Test of the Validator itself.
     *
     * Tests validating a known good resource, checking that it succeeds.
     *
     */
    @Test
    public void testValidator_Pass_Object() {
        System.out.println("testValidator_Pass_Object(Loads a good resource into an object)");
        try {
            theGoodResource = IOUtils.toString(
                    this.getClass().getResourceAsStream("patient-example.xml"),
                    "UTF-8"
            );
        }
        catch (IOException ex) {
            Logger.getLogger(MyValidatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        FhirContext newCtx = FhirContext.forDstu3();
        IBaseResource resource = newCtx.newXmlParser().parseResource(theGoodResource);
        MyValidator instance = MyValidator.getInstance(newCtx);
        ValidationResult res = instance.validateWithResult(resource);
        assertEquals(true, res.isSuccessful());
    }

    /**
     * Test of the Validator itself. This tests a patient resource with two
     * birthDate elements, which breaks cardinality rules.
     *
     */
//    @Test
//    public void testValidator_Fail_Object() {
//        System.out.println("testValidator_Fail_Object");
//        try {
//            theBadResource = IOUtils.toString(
//                this.getClass().getResourceAsStream("bad-patient-example.xml"),
//                "UTF-8"
//            );
//        } catch (IOException ex) {
//            Logger.getLogger(MyValidatorTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        FhirContext newCtx = FhirContext.forDstu3();
//        IBaseResource resource = newCtx.newXmlParser().parseResource(theBadResource);
//        MyValidator instance = MyValidator.getInstance(newCtx);
//        ValidationResult res = instance.validateWithResult(resource);
//        assertEquals(false, res.isSuccessful());
//        assertEquals(2, res.getMessages().size());
//    }

    /**
     * Test of the Validator itself. This tests Parameters resource with two
     * birthDate elements, which breaks cardinality rules.
     *
     */
    /*
    @Test
    public void testValidator_Fail_ParamAsString() {
        System.out.println("testValidator_Fail_ParamAsString (Load a Parameters resource as a String)");
        try {
            theBadResource = IOUtils.toString(
                    this.getClass().getResourceAsStream("BadParameter.xml"),
                    "UTF-8"
            );
        }
        catch (IOException ex) {
            Logger.getLogger(MyValidatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        FhirContext newCtx = FhirContext.forDstu3();
        MyValidator instance = MyValidator.getInstance(newCtx);
        ValidationResult res = instance.validateWithResult(theBadResource);
        // Assert that it failed validation
        assertEquals(false, res.isSuccessful());

        // Assert thee were two problems
        assertEquals(2, res.getMessages().size());

        SingleValidationMessage errorMsg0 = res.getMessages().get(0);
        SingleValidationMessage errorMsg1 = res.getMessages().get(1);

        // Assert the location of the first problem
        assertEquals(errorMsg0.getLocationLine().intValue(), 80);
        assertEquals(errorMsg0.getLocationCol().intValue(), 35);

        // Assert it was an ERROR
        assertEquals(errorMsg0.getSeverity(), ResultSeverityEnum.ERROR);

        // Assert the location of the second problem
        assertEquals(errorMsg1.getLocationLine().intValue(), 0);
        assertEquals(errorMsg1.getLocationCol().intValue(), 0);

        // Assert it was an ERROR
        assertEquals(errorMsg1.getSeverity(), ResultSeverityEnum.ERROR);

        // Assert the second problem's message
        assertEquals(failMsg, errorMsg1.getMessage());
    }
    */

    /**
     * Test of the Validator itself.
     *
     * Tests validating a known good Parameters resource, checking that it succeeds.
     *
     */
    /*
    @Test
    public void testValidator_Pass_ParamAsString() {
        System.out.println("testValidator_Pass_String");
        try {
            theGoodResource = IOUtils.toString(
                    this.getClass().getResourceAsStream("Parameter.xml"),
                    "UTF-8"
            );
        }
        catch (IOException ex) {
            Logger.getLogger(MyValidatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        FhirContext newCtx = FhirContext.forDstu3();
        MyValidator instance = MyValidator.getInstance(newCtx);
        ValidationResult res = instance.validateWithResult(theGoodResource);
        System.out.println(res.getMessages().size());
        assertEquals(true, res.isSuccessful());
    }
    */
    
}
