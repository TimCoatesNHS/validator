/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.validator.ResourceProvider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.ValidationModeEnum;
import java.io.IOException;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import ca.uhn.fhir.parser.IParser;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hl7.fhir.dstu3.model.Contract;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.OperationOutcome;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.instance.model.api.IBaseOperationOutcome;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.junit.After;
import static org.mockito.Mockito.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tim.coates@nhs.net
 */
public class GenericResourceProviderTest {

    private static final Logger LOG = Logger.getLogger(GenericResourceProviderTest.class.getName());

    FhirContext theContext;

    public GenericResourceProviderTest() {
        theContext = FhirContext.forDstu3();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getResourceType method, of class GenericResourceProvider.
     */
    @Test
    public void testGetResourceType() {
        System.out.println("getResourceType");
        GenericResourceProvider instance = new GenericResourceProvider(theContext, Contract.class);
        Class<? extends IBaseResource> expResult = Contract.class;
        Class<? extends IBaseResource> result = instance.getResourceType();
        assertEquals(expResult, result);
    }

    /**
     * Test of validateResource method, of class GenericResourceProvider.
     *
     * Loads a known good resource and validates it against the HL7 profile.
     *
     * Loaded profile has no <meta> section, so also checks that trying to get
     * the profile from there doesn't fail.
     *
     * Should return no OperationOutcome (null).
     *
     */
    @Test
    public void testValidateResource_0() {
        System.out.println("validateResource_0");
        IBaseResource theResource = null;
        MethodOutcome result = null;
        HttpServletRequest theRequest = null;
        HttpServletResponse theResponse = null;

        try {
            String theGoodResource = IOUtils.toString(
                    this.getClass().getResourceAsStream("patient-example.xml"),
                    "UTF-8"
            );
            IParser parser = theContext.newXmlParser();
            theResource = (Patient) parser.parseResource(theGoodResource);
            ValidationModeEnum theMode = ValidationModeEnum.CREATE;
            // profileName is passed inthe format:
            // UriType[https://fhir.nhs.uk/STU3/StructureDefinition/NDOP-Consent-1]
            String profileName = "UriType[https://hl7.org/fhir/Patient]";
            GenericResourceProvider instance = new GenericResourceProvider(theContext, Patient.class);

            result = instance.validateResource(theResource, theMode, profileName, theRequest, theResponse);
            OperationOutcome oo = (OperationOutcome) result.getOperationOutcome();

            if (oo != null) {
                if (oo.hasIssue()) {
                    List<OperationOutcome.OperationOutcomeIssueComponent> issues = oo.getIssue();
                    for (OperationOutcome.OperationOutcomeIssueComponent issue : issues) {
                        LOG.severe(issue.getDetails().getText());
                    }
                }
            }
            assertNull(oo);
        }
        catch (IOException ex) {
            LOG.severe("IOException reading tests resource file: patient-example.xml");
            fail("Exception caught");
        }
    }

    /**
     * Test of validateResource method, of class GenericResourceProvider.
     *
     * Tests that when no <meta><profile> is supplied, or in QueryString one is
     * supplied in parameters, then the resource is validated OK.
     *
     */
    @Test
    public void testValidateResource_1() {
        System.out.println("validateResource_1");
        IBaseResource theResource = null;
        MethodOutcome result = null;
        HttpServletRequest theRequest = null;
        HttpServletResponse theResponse = null;

        try {
            String theGoodResource = IOUtils.toString(
                    this.getClass().getResourceAsStream("patient-example.xml"),
                    "UTF-8"
            );
            IParser parser = theContext.newXmlParser();
            theResource = (Patient) parser.parseResource(theGoodResource);
            ValidationModeEnum theMode = null;
            String profileName = "UriType[https://www.hl7.org/fhir/Patient]";
            GenericResourceProvider instance = new GenericResourceProvider(theContext, Patient.class);

            result = instance.validateResource(theResource, theMode, profileName, theRequest, theResponse);

        }
        catch (IOException ex) {
            LOG.severe("IOException reading tests resource file: patient-example.xml");
        }
        // TODO review the generated test code and remove the default call to fail.
        IBaseOperationOutcome oo = result.getOperationOutcome();
        assertNull(oo);
    }

    /**
     * Test of validateResource method, of class GenericResourceProvider.
     *
     * This tests that a resource is validated where it has a good profile in
     * <meta><profile> AND in QueryString AND in Parameters.
     *
     */
    @Test
    public void testValidateResource_2() {
        System.out.println("validateResource_2");
        IBaseResource theResource = null;
        MethodOutcome result = null;
        HttpServletRequest theRequest = mock(HttpServletRequest.class);
        HttpServletResponse theResponse = null;

        when(theRequest.getQueryString()).thenReturn("profile=https://www.hl7.org/fhir/Patient&_format=application/fhir+xml");

        try {
            String theGoodResource = IOUtils.toString(
                    this.getClass().getResourceAsStream("patient-example_withmetaprofile.xml"),
                    "UTF-8"
            );
            IParser parser = theContext.newXmlParser();
            theResource = (Patient) parser.parseResource(theGoodResource);
            ValidationModeEnum theMode = ValidationModeEnum.CREATE;
            String profileName = "UriType[https://www.hl7.org/fhir/Patient]";
            GenericResourceProvider instance = new GenericResourceProvider(theContext, Patient.class);

            result = instance.validateResource(theResource, theMode, profileName, theRequest, theResponse);

        }
        catch (IOException ex) {
            LOG.severe("IOException reading tests resource file: patient-example_withmetaprofile.xml" + ex.getMessage());
        }
        // TODO review the generated test code and remove the default call to fail.
        IBaseOperationOutcome oo = result.getOperationOutcome();
        assertNull(oo);
    }

    /**
     * Test of validateResource method, of class GenericResourceProvider.
     *
     * This tests that despite the resource having a wrong profile name in
     * <meta><profile> and also a wrong one supplied in QueryString, it still
     * validates successfully given a good one in Parameters.
     *
     */
    @Test
    public void testValidateResource_3() {
        System.out.println("validateResource_3");
        IBaseResource theResource = null;
        MethodOutcome result = null;
        HttpServletRequest theRequest = mock(HttpServletRequest.class);
        HttpServletResponse theResponse = null;

        when(theRequest.getQueryString()).thenReturn("profile=https://www.hl7.org/fhir/PatientX&_format=application/fhir+xml");

        try {
            String theGoodResource = IOUtils.toString(
                    this.getClass().getResourceAsStream("patient-example_withBADmetaprofile.xml"),
                    "UTF-8"
            );
            IParser parser = theContext.newXmlParser();
            theResource = (Patient) parser.parseResource(theGoodResource);
            ValidationModeEnum theMode = ValidationModeEnum.CREATE;
            String profileName = "UriType[https://www.hl7.org/fhir/Patient]";
            GenericResourceProvider instance = new GenericResourceProvider(theContext, Patient.class);

            result = instance.validateResource(
                    theResource,
                    theMode,
                    profileName,
                    theRequest,
                    theResponse);

        }
        catch (IOException ex) {
            LOG.severe("IOException reading tests resource file: patient-example_withBADmetaprofile.xml" + ex.getMessage());
        }
        // TODO review the generated test code and remove the default call to fail.
        IBaseOperationOutcome oo = result.getOperationOutcome();
        assertNull(oo);
    }

    /**
     * Test of validateResource method, of class GenericResourceProvider.
     *
     * This tests that despite having a dodgy profile in <meta><profile>,
     * because we supplied a sensible one in parameters it will be used, and the
     * resource will validate.
     *
     * TODO: Probably need to add a feature to mention the dodgy profile?
     *
     */
    @Test
    public void testValidateResource_4() {
        System.out.println("validateResource_4");
        IBaseResource theResource = null;
        MethodOutcome result = null;
        HttpServletRequest theRequest = null;
        HttpServletResponse theResponse = null;

        try {
            String theGoodResource = IOUtils.toString(
                    this.getClass().getResourceAsStream("patient-example_withBADmetaprofile.xml"),
                    "UTF-8"
            );
            IParser parser = theContext.newXmlParser();
            theResource = (Patient) parser.parseResource(theGoodResource);
            ValidationModeEnum theMode = ValidationModeEnum.CREATE;
            String profileName = "UriType[https://www.hl7.org/fhir/Patient]";
            GenericResourceProvider instance = new GenericResourceProvider(theContext, Patient.class);

            result = instance.validateResource(
                    theResource,
                    theMode,
                    profileName,
                    theRequest,
                    theResponse);

        }
        catch (IOException ex) {
            LOG.severe("IOException reading tests resource file: patient-example_withBADmetaprofile.xml" + ex.getMessage());
        }
        // TODO review the generated test code and remove the default call to fail.
        IBaseOperationOutcome oo = result.getOperationOutcome();
        assertNull(oo);
    }

    /**
     * Test of getResourceById method, of class GenericResourceProvider.
     *
     * Frankly this tests that the @Read operation doesn't return anything.
     *
     * Test passes if an attempt to read a resource returns null.
     */
    @Test
    public void testGetResourceById() {
        System.out.println("getResourceById");
        IdType theId = new IdType("12345");
        GenericResourceProvider instance = new GenericResourceProvider(theContext, Patient.class);
        IBaseResource expResult = null;
        IBaseResource result = instance.getResourceById(theId);
        assertEquals(expResult, result);
    }

    /**
     * Test of setProfileToUse method, of class GenericResourceProvider.
     *
     * This test uses a resource with profile in meta/profile and nothing else.
     */
    @Test
    public void testSetProfileToUse_0() {
        System.out.println("setProfileToUse_0 (<meta><profile> test)");
        IBaseResource theResource = null;
        GenericResourceProvider instance = new GenericResourceProvider(theContext, Patient.class);
        try {
            String theGoodResource = IOUtils.toString(
                    this.getClass().getResourceAsStream("patient-example_withmetaprofile.xml"),
                    "UTF-8"
            );
            IParser parser = theContext.newXmlParser();
            theResource = (Patient) parser.parseResource(theGoodResource);

        }
        catch (IOException ex) {
            LOG.severe("IOException reading tests resource file: patient-example.xml" + ex.getMessage());
        }

        String profileName = "";
        HttpServletRequest theRequest = mock(HttpServletRequest.class);

        when(theRequest.getQueryString()).thenReturn(null);

        String expResult = "https://hl7.org/fhir/Patient";
        IBaseResource result = instance.setProfileToUse(theResource, profileName, theRequest);

        String resultProfile = result.getMeta().getProfile().get(0).getValue();
        assertEquals(expResult, resultProfile);
    }

    /**
     * Test of setProfileToUse method, of class GenericResourceProvider.
     *
     * This test uses a resource with profile in meta/profile and in QueryString
     * <meta><profile> has: https://www.hl7.org/fhir/Patient QueryString has:
     * https://www.hl7.org/fhir/Patient1 Test passes if QueryString used in
     * preference to <meta><profile>
     */
    @Test
    public void testSetProfileToUse_1() {
        System.out.println("setProfileToUse_1 (QueryString test)");
        IBaseResource theResource = null;
        GenericResourceProvider instance = new GenericResourceProvider(theContext, Patient.class);
        try {
            String theGoodResource = IOUtils.toString(
                    this.getClass().getResourceAsStream("patient-example_withmetaprofile.xml"),
                    "UTF-8"
            );
            IParser parser = theContext.newXmlParser();
            theResource = (Patient) parser.parseResource(theGoodResource);

        }
        catch (IOException ex) {
            LOG.severe("IOException reading tests resource file: patient-example.xml" + ex.getMessage());
        }

        String profileName = "";
        HttpServletRequest theRequest = mock(HttpServletRequest.class);

        when(theRequest.getQueryString()).thenReturn("profile=https://www.hl7.org/fhir/Patient1&_format=application/fhir+xml");

        String expResult = "https://www.hl7.org/fhir/Patient1";
        IBaseResource result = instance.setProfileToUse(theResource, profileName, theRequest);

        String resultProfile = result.getMeta().getProfile().get(0).getValue();
        assertEquals(expResult, resultProfile);
    }

    /**
     * Test of setProfileToUse method, of class GenericResourceProvider.
     *
     * This test uses a resource with profile in meta/profile, QueryString and
     * supplied as a parameter.
     *
     * <meta><profile> has: https://www.hl7.org/fhir/Patient QueryString has:
     * https://www.hl7.org/fhir/Patient1 Parameters has:
     * https://www.hl7.org/fhir/Patient2 Test passes if Parameter used.
     *
     */
    @Test
    public void testSetProfileToUse_2() {
        System.out.println("setProfileToUse_2 (Parameters test)");
        IBaseResource theResource = null;
        GenericResourceProvider instance = new GenericResourceProvider(theContext, Patient.class);
        try {
            String theGoodResource = IOUtils.toString(
                    this.getClass().getResourceAsStream("patient-example_withmetaprofile.xml"),
                    "UTF-8"
            );
            IParser parser = theContext.newXmlParser();
            theResource = (Patient) parser.parseResource(theGoodResource);

        }
        catch (IOException ex) {
            LOG.severe("IOException reading tests resource file: patient-example.xml" + ex.getMessage());
        }
        String profileName = "UriType[https://www.hl7.org/fhir/Patient2]";
        HttpServletRequest theRequest = mock(HttpServletRequest.class);

        when(theRequest.getQueryString()).thenReturn("profile=https://www.hl7.org/fhir/Patient1&_format=application/fhir+xml");

        String expResult = "https://www.hl7.org/fhir/Patient2";
        IBaseResource result = instance.setProfileToUse(theResource, profileName, theRequest);

        String resultProfile = result.getMeta().getProfile().get(0).getValue();
        assertEquals(expResult, resultProfile);
    }
}
